import React from "react";
import PageWrapper from "../components/PageWrapper";
import Home from "../sections/instanAja/Home";
import Content1 from "../sections/instanAja/Content1";
import Content2 from "../sections/instanAja/Content2";
import Process from "../sections/instanAja/Process";
import Video from "../sections/instanAja/Video";
import Features from "../sections/instanAja/Features";
import Testimonials from "../sections/instanAja/Testimonials";
import Brand from "../sections/instanAja/Brand";

const MobileApp = () => {
  return (
    <>
      <PageWrapper
        headerConfig={{
          theme: "dark",
          align: "right",
          variant: "danger",
          isFluid: true,
          button: "null", // cta, account, null
        }}
        footerConfig={{
          style: "styleLogin", //style1, style2
        }}
      >
        <Home />
        <Content1 />
        <Content2 />
        <Process />
        <div className="gradient-sections-wrapper bg-gradient-1">
          <Video />
          <Features />
          <Testimonials />
          <Brand />
        </div>
        
      </PageWrapper>
    </>
  );
};
export default MobileApp;
