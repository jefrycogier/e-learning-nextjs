import React from "react";
import { Container, Row, Col } from "react-bootstrap";

import imgC1 from "../../assets/image/instanAja/png/categories.png";

const Content = () => {
  return (
    <>
      {/* <!-- Content Area --> */}
      <div className="content-section pt-6 pt-lg-19 pb-5 bg-default-4">
        <Container>
          <Row className="align-items-center">
            <Col lg="6" data-aos="flip-right" data-aos-duration="1000">
              <div className="content-img">
                <img src={imgC1} alt="" className="w-100" />
              </div>
            </Col>
            <Col xs="10" lg="5" className="pl-xl-21">
              <div className="section-title content-text mb-13">
                <h2 className="title gr-text-3 mb-6">
                Kategori Kelas Beragam
                </h2>
                <p className="gr-text-8">
                Ikuti beragam kategori kelas dengan metode belajar mudah dari praktisi profesional</p>
              </div>
              <div className="content-widget">
                <Row>
                  <Col
                    xs="6"
                    lg="6"
                    data-aos="fade-left"
                    data-aos-duration="750"
                  >
                    <div className="single-widget mb-9">
                      <p className="gr-text-9 mb-0">
                      <ul>
                        <li>Bisnis</li>
                        <li>Kecantikan</li>
                        <li>Keuangan</li>
                        <li>Media</li>
                        <li>Pengembangan Diri</li>
                        <li>Bahasa</li>
                        <li>Fotografi</li>
                      </ul>
                      </p>
                    </div>
                  </Col>
                  <Col
                    xs="6"
                    lg="6"
                    data-aos="fade-left"
                    data-aos-duration="1100"
                  >
                    <div className="single-widget mb-9">
                      <p className="gr-text-9 mb-0">
                      <ul>
                        <li>Perhotelan</li>
                        <li>Penjualan</li>
                        <li>Ilmu Komputer</li>
                        <li>Tata Boga</li>
                        <li>Barista</li>
                        <li>Arsitektur</li>
                        <li>Keperawatan</li>
                      </ul>
                      </p>
                    </div>
                  </Col>
                </Row>
              </div>
            </Col>
          </Row>
        </Container>
      </div>
    </>
  );
};

export default Content;
