import React from "react";
import { Container, Row, Col } from "react-bootstrap";

import imgF1 from "../../assets/image/svg/l6-feature-icon1.svg";
import imgF2 from "../../assets/image/svg/l6-feature-icon2.svg";
import imgF3 from "../../assets/image/svg/l6-feature-icon3.svg";
import imgF4 from "../../assets/image/svg/l6-feature-icon4.svg";
import imgF5 from "../../assets/image/svg/l6-feature-icon5.svg";
import imgF6 from "../../assets/image/svg/l6-feature-icon6.svg";

const Features = () => {
  return (
    <>
      {/* <!-- Features Area --> */}
      <div className="feature-section pt-16 pt-lg-25 dark-mode-texts">
        <Container>
          <Row className="justify-content-center">
            <Col md="8" lg="7" xl="6">
              <div className="section-title text-center mb-13 mb-lg-23">
                <h2 className="title gr-text-5">
                Mulai Karirmu di Instan Aja
                </h2>
                <p className="gr-text-10 gr-text-color-opacity text-green">
                  Instan Aja teman belajarmu
                </p>
              </div>
            </Col>
          </Row>
          <Row className="justify-content-center">
            <Col sm="6" lg="4" className="mb-12">
              <div className="feature-widget d-sm-flex">
                <div className="widget-icon gr-text-4 text-green mr-sm-9 mb-7 mb-lg-0">
                  <img className="" src={imgF1} alt="/" />
                </div>
                <div className="content">
                  <h3 className="title gr-text-7 mb-5">Belajar Skill Interaktif</h3>
                  <p className="gr-text-9 gr-text-color-opacity">
                  Kelas Streaming & interaktif dengan pengajar dan peserta lain.
                  </p>
                </div>
              </div>
            </Col>
            <Col sm="6" lg="4" className="mb-12">
              <div className="feature-widget d-sm-flex">
                <div className="widget-icon gr-text-4 text-green mr-sm-9 mb-7 mb-lg-0">
                  <img className="" src={imgF2} alt="/" />
                </div>
                <div className="content">
                  <h3 className="title gr-text-7 mb-5">Pengajar Profesional</h3>
                  <p className="gr-text-9 gr-text-color-opacity">
                  Dibimbing oleh para pengajar profesional dan berpengalaman.
                  </p>
                </div>
              </div>
            </Col>
            <Col sm="6" lg="4" className="mb-12">
              <div className="feature-widget d-sm-flex">
                <div className="widget-icon gr-text-4 text-green mr-sm-9 mb-7 mb-lg-0">
                  <img className="" src={imgF3} alt="/" />
                </div>
                <div className="content">
                  <h3 className="title gr-text-7 mb-5">Pengumpulan Tugas</h3>
                  <p className="gr-text-9 gr-text-color-opacity">
                  Tugas dapat dikumpulkan dengan mengunggahnya di aplikasi.
                  </p>
                </div>
              </div>
            </Col>
            <Col sm="6" lg="4" className="mb-12">
              <div className="feature-widget d-sm-flex">
                <div className="widget-icon gr-text-4 text-green mr-sm-9 mb-7 mb-lg-0">
                  <img className="" src={imgF4} alt="/" />
                </div>
                <div className="content">
                  <h3 className="title gr-text-7 mb-5">Video On Demand</h3>
                  <p className="gr-text-9 gr-text-color-opacity">
                  Kelas online akan direkam dan dapat diikuti kapan saja.
                  </p>
                </div>
              </div>
            </Col>
            <Col sm="6" lg="4" className="mb-12">
              <div className="feature-widget d-sm-flex">
                <div className="widget-icon gr-text-4 text-green mr-sm-9 mb-7 mb-lg-0">
                  <img className="" src={imgF5} alt="/" />
                </div>
                <div className="content">
                  <h3 className="title gr-text-7 mb-5">Penjadwalan Kelas</h3>
                  <p className="gr-text-9 gr-text-color-opacity">
                  Kelas yang dipilih dapat dijadwalkan.
                  </p>
                </div>
              </div>
            </Col>
            <Col sm="6" lg="4" className="mb-12">
              <div className="feature-widget d-sm-flex">
                <div className="widget-icon gr-text-4 text-green mr-sm-9 mb-7 mb-lg-0">
                  <img className="" src={imgF6} alt="/" />
                </div>
                <div className="content">
                  <h3 className="title gr-text-7 mb-5">Pembayaran Mudah</h3>
                  <p className="gr-text-9 gr-text-color-opacity">
                  Pembayaran kelas dapat dilakukan dengan mudah dengan berbagai akun bank.
                  </p>
                </div>
              </div>
            </Col>
            <div className="col-12">
              <div className="border-sm-divider dark text-center mt-lg-13"></div>
            </div>
          </Row>
        </Container>
      </div>
    </>
  );
};

export default Features;
